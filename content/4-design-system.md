#Design system
目前提供light theme。

------

##Color
  >>"color-primary-100": "#FEFCDA",  
  >>"color-primary-200": "#FEF8B5",  
  >>"color-primary-300": "#FDF390",  
  >>"color-primary-400": "#FBED74",  
  >>"color-primary-500": "#F9E547",  
  >>"color-primary-600": "#D6C233",  
  >>"color-primary-700": "#B39F23",  
  >>"color-primary-800": "#907E16",  
  >>"color-primary-900": "#77660D",  
  >>"color-primary-transparent-100": "rgba(249, 229, 71, 0.08)",  
  >>"color-primary-transparent-200": "rgba(249, 229, 71, 0.16)",  
  >>"color-primary-transparent-300": "rgba(249, 229, 71, 0.24)",  
  >>"color-primary-transparent-400": "rgba(249, 229, 71, 0.32)",  
  >>"color-primary-transparent-500": "rgba(249, 229, 71, 0.4)",  
  >>"color-primary-transparent-600": "rgba(249, 229, 71, 0.48)",  
  
  >>"color-success-100": "#DFFDDA",  
  >>"color-success-200": "#B9FBB6",  
  >>"color-success-300": "#90F496",  
  >>"color-success-400": "#72EA85",  
  >>"color-success-500": "#46DD6C",  
  >>"color-success-600": "#33BE63",  
  >>"color-success-700": "#239F5A",  
  >>"color-success-800": "#16804F",  
  >>"color-success-900": "#0D6A48",  
  >>"color-success-transparent-100": "rgba(70, 221, 108, 0.08)",  
  >>"color-success-transparent-200": "rgba(70, 221, 108, 0.16)",  
  >>"color-success-transparent-300": "rgba(70, 221, 108, 0.24)",  
  >>"color-success-transparent-400": "rgba(70, 221, 108, 0.32)",  
  >>"color-success-transparent-500": "rgba(70, 221, 108, 0.4)",  
  >>"color-success-transparent-600": "rgba(70, 221, 108, 0.48)",  
  
  >>"color-info-100": "#CEF5FF",  
  >>"color-info-200": "#9CE7FF",  
  >>"color-info-300": "#6CD2FF",  
  >>"color-info-400": "#47BDFF",  
  >>"color-info-500": "#0A99FF",  
  >>"color-info-600": "#0776DB",  
  >>"color-info-700": "#0558B7",  
  >>"color-info-800": "#033E93",  
  >>"color-info-900": "#012C7A",  
  >>"color-info-transparent-100": "rgba(10, 153, 255, 0.08)",  
  >>"color-info-transparent-200": "rgba(10, 153, 255, 0.16)",  
  >>"color-info-transparent-300": "rgba(10, 153, 255, 0.24)",  
  >>"color-info-transparent-400": "rgba(10, 153, 255, 0.32)",  
  >>"color-info-transparent-500": "rgba(10, 153, 255, 0.4)",  
  >>"color-info-transparent-600": "rgba(10, 153, 255, 0.48)",  
  
  >>"color-warning-100": "#FFF7D8",  
  >>"color-warning-200": "#FFEDB1",  
  >>"color-warning-300": "#FFE18A",  
  >>"color-warning-400": "#FFD56D",  
  >>"color-warning-500": "#FFC13D",  
  >>"color-warning-600": "#DB9D2C",  
  >>"color-warning-700": "#B77C1E",  
  >>"color-warning-800": "#935E13",  
  >>"color-warning-900": "#7A480B",  
  >>"color-warning-transparent-100": "rgba(255, 193, 61, 0.08)",  
  >>"color-warning-transparent-200": "rgba(255, 193, 61, 0.16)",  
  >>"color-warning-transparent-300": "rgba(255, 193, 61, 0.24)",  
  >>"color-warning-transparent-400": "rgba(255, 193, 61, 0.32)",  
  >>"color-warning-transparent-500": "rgba(255, 193, 61, 0.4)",  
  >>"color-warning-transparent-600": "rgba(255, 193, 61, 0.48)",  
  
  >>"color-danger-100": "#FFDCDD",  
  >>"color-danger-200": "#FFBAC3",  
  >>"color-danger-300": "#FF98B0",  
  >>"color-danger-400": "#FF7EA9",  
  >>"color-danger-500": "#FF549E",  
  >>"color-danger-600": "#DB3D91",  
  >>"color-danger-700": "#B72A83",  
  >>"color-danger-800": "#931A73",  
  >>"color-danger-900": "#7A1068",  
  
  >>"color-danger-transparent-100": "rgba(255, 84, 158, 0.08)",  
  >>"color-danger-transparent-200": "rgba(255, 84, 158, 0.16)",  
  >>"color-danger-transparent-300": "rgba(255, 84, 158, 0.24)",  
  >>"color-danger-transparent-400": "rgba(255, 84, 158, 0.32)",  
  >>"color-danger-transparent-500": "rgba(255, 84, 158, 0.4)",  
  >>"color-danger-transparent-600": "rgba(255, 84, 158, 0.48)"

}
##Backgrounds & Borders  
透過資訊彙整，建立亞洲最活躍的訂閱者、創作者互動平台。  
透過 FLOW NFT，落實知識性數位資產的商業模式。  
##Text Colors  
###Fonts & Text Styles  


